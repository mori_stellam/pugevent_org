# Sandstorm PUG Lounge's PUG Event organizer.

SPL's own bot to organize PUG events. At the moment, only signing up and ready-check is handled. _The bot is work in progress, additional functions are continuously developed._

## How do events work?

At the moment an event may consist of five stages:
1. Sign-up stage: Discord users can indicate if they participate in the event. They can choose `participate`, `maybe`, `can't participate`.
2. Ready-check stage: At the beginning of the ready-check phase, Discord users will get a direct message as a call to ready-check in the event. Users who indicated `maybe` during the sign-up phase will be moved to the non-participants in this stage, but will receive a DM. Users can choose `participatant`, `participant as a substitute`, or `can't participate`.
3. Deadline for joining the event, including ready-check, is over.

## How to use the bot?

First use `/createevent` command to create an event, then use the `/seteventdescription` and `/addeventdescription` commands to provide an event description.


## Code reference / Slash commands

_Note: At the moment commands can by invoked by ANYONE in ANY channel, regardless of Discord roles._
- `/addmodrole @DISCORDROLE` - Specifies the Discord role required to access bot commands (_not enforced yet_).
- `/createevent ENAME EDATE [EDEADLINE]` - Creates a new event with name `ENAME`. The final deadline for signing-up (including reaching) is specified by `EDATE`. `EDATE` has to follow the `%d-%m-%y %H:%M` format (`%d`: two-digit day; `%m`: two-digit month; `%y`: two-digit year, `%H`: two-digit hour between 01 and 23, `%M`: two-digit minute 00-59). For instance `01-12-45 20:59` is December 1st, 2045, 8:59 pm. `EDEADLINE` is an _OPTIONAL_ argument following the same date-time format, which indicates the beginning of the ready-check phase (by default is 24 hours before `EDATE`). _Note: You can indicate the date-time of the event in the description of the event. See `/seteventdescription` and `/addeventdescription`._
- `/seteventdescription EVENTID DESCRIPTION` - Changes the description of the event with ID `EVENTID`. The new description of the event will be `DESCRIPTION`.
- `/addeventdescription EVENTID DESCRIPTION` - This command adds to the description of event (with ID `EVENTID`). `DESCRIPTION` will be added to the existing description separated by a new line.
- `/cancelEvent EVENTID` - Cancels an event specified by the `EVENTID` and deletes the corresponding message.
