import logging
from datetime import datetime
from dbmodules import config as pconfig

logger = logging.getLogger('DCBOT')

class Event:
    def __init__(self, *args):
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
            self.constructor(*values)
        elif len(args)>=4:
            self.constructor(*args)
        else:
            logging.error("Error in EVENT constructor: Mismatching arguments.")
        return None

    def constructor(self, id: int, name: str, date: str,
                    deadline: str, descr: str="", org: int=0,
                    sign: str="", mayb: str="", abs: str="", cats: str="", status: int=0):
        self.eventid = id
        self.name = name
        self.date = date
        self.deadline = deadline
        self.desc = descr
        self.org = org
        self.signup = sign
        self.maybe = mayb
        self.absent = abs
        self.categs = cats
        self.status = status
        return None

    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]

class Categories:
    def __init__(self, *args):
        if len(args)==1 and isinstance(args[0], dict):
            values = [v for k, v in args[0].items()]
            self.constructor(*values)
        elif len(args)>=3:
            self.constructor(*args)
        else:
            logging.error("Error in CATEGORY constructor: Mismatching arguments.")
        return None
    
    def constructor(self, id: int, eventid: int, name: str, elems: str=""):
        self.catid = id
        self.eventid = eventid
        self.name = name
        self.elements = elems
        return None
    
    def getQueryStr(self):
        """ Return query string of object
        """
        keys = "(" + ','.join([key for key in self.__dict__.keys()]) +  ")"
        values = "(" + \
                ','.join(["'"+str(key)+"'" for key in self.__dict__.values()]) + \
                ")"
        return [keys, values]

class EventQueueItem:
    def __init__(self, *args):
        if len(args)==1:
            [cid, mid, eid] = args[0].split(";")
            self.setItem(int(cid), int(mid), int(eid))
        elif len(args)==3:
            self.setItem(*args)
        else:
            print("Error in event queue item.")
        return None
    
    def setItem(self, channelid: int, messageid: int, eventid: int):
        self.channelid = channelid
        self.messageid = messageid
        self.eventid = eventid

    def getStoreString(self):
        return f"{str(self.channelid)};{str(self.messageid)};{str(self.eventid)}"
    
    def __eq__(self, other):
        return (self.channelid==other.channelid) and (self.messageid==other.messageid) and (self.eventid==other.eventid)
    