import configparser

parsedParams = configparser.ConfigParser()
parsedParams.read('config.ini')

# Database and bot config
dbname = str(parsedParams["BOT"]["dbname"])
datetimeFmt = str(parsedParams["BOT"]["datetimeFmt"])
timeSleep = int(parsedParams["BOT"]["timeSleep"])
embedUrls = str(parsedParams["BOT"]["embedUrl"])
readyCheckDays = int(parsedParams["BOT"]["readycheckdays"])
IANAZone = str(parsedParams["BOT"]["ianazoneinfo"])

# Discord Server
GuildID = int(parsedParams["DCSERVER"]["DCServerID"])
AdminRole = int(parsedParams["DCSERVER"]["AdminRole"])