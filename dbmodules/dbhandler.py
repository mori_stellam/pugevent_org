import aiosqlite
import asyncio
import functools
import logging
import os
from datetime import datetime
from contextlib import asynccontextmanager

from dbmodules import config as pconfig
from dbmodules import classes as pclasses
from bot import constants

logger = logging.getLogger('DCBOT')

# Functions to establish connection with database
async def dbinit_initTables(con: aiosqlite.Connection):
    """ Function to create the tables to the newly initialized db
    """

    await con.execute(""" CREATE TABLE events
                        (eventid integer, name text, date text, deadline text,
                        desc text, org integer, signup text, maybe text, absent text,
                        categs text, status integer)
                    """)
    await con.execute(""" CREATE TABLE categories
                        (catid integer, eventid integer, name text, elements text)""")
    await con.commit()
    return None

def dbdict_factory(cursor: aiosqlite.Cursor, row):
    """ Support function for dbinit_connection to return dictionaries from queries
    """
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

async def dbinit_connection(name: str, reinit: bool=False):
    """ Function to return sqlite connection
    """
    if reinit==True:
        try:
            os.remove(name)
        except:
            pass
    flag = os.path.isfile(name)
    con = await aiosqlite.connect(name)
    con.row_factory = dbdict_factory
    if not flag:
        print("Init db file")
        await dbinit_initTables(con)
    return con

# DB connection decorator
@asynccontextmanager
async def dbConnectContext(name: str):
    logging.info(f"{name}")
    con = await dbinit_connection(name, reinit=False)
    try:
        yield con
    finally:
        await con.close()

def dbopenconnect_decor(f):
    @functools.wraps(f)
    async def wrapped_func(*args, **kwargs):
        async with dbConnectContext(pconfig.dbname) as conn:
            if not asyncio.iscoroutinefunction(f):
                return f(conn, *args, **kwargs)
            else:
                return await f(conn, *args, **kwargs)
    return wrapped_func



# Basic queries and update commands
@dbopenconnect_decor
async def db_addEvent(lcon, event: pclasses.Event):
    lcurs = await lcon.execute(f"SELECT * FROM events WHERE eventid='{event.eventid}'")
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        [keys, values] = event.getQueryStr()
        await lcon.execute(f"INSERT INTO events {keys} VALUES {values};")
        await lcon.commit()
    return None

@dbopenconnect_decor
async def db_addCat(lcon, cat: pclasses.Categories):
    lcurs = await lcon.execute(f"SELECT * FROM categories WHERE catid='{cat.catid}'")
    fetch = await lcurs.fetchall()
    if len(fetch)==0:
        [keys, values] = cat.getQueryStr()
        await lcon.execute(f"INSERT INTO categories {keys} VALUES {values}")
        await lcon.commit()
    return None

@dbopenconnect_decor
async def db_getEvent(lcon, eventid: int, feedback = True):
    lcurs = await lcon.execute(f""" SELECT * FROM events WHERE eventid='{eventid}'""")
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnEvent = pclasses.Event(fetch[0])
    else:
        returnEvent = None
        if feedback:
            logger.info(f"MSG : Could not find event with ID {eventid}.")
    return returnEvent

@dbopenconnect_decor
async def db_getCat(lcon, catid: int):
    lcurs = await lcon.execute(f""" SELECT * FROM categories WHERE catid='{catid}'""")
    fetch = await lcurs.fetchall()
    if len(fetch)>0:
        returnCategory = pclasses.Categories(fetch[0])
    else:
        returnCategory = None
        logger.info(f"MSG : Could not find category with ID {catid}")
    return returnCategory

@dbopenconnect_decor
async def db_getAllCats(lcon):
    lcurs = await lcon.execute(f""" SELECT * FROM categories""")
    fetch = await lcurs.fetchall()
    retlist = []
    if len(fetch)>0:
        for fetchItem in fetch:
            retlist.append(pclasses.Categories(fetchItem))
    return retlist


@dbopenconnect_decor
async def db_updateEvent(lcon, event: pclasses.Event):
    lcurs = await lcon.execute(f""" UPDATE events
                                    SET name='{event.name}',
                                        date='{event.date}',
                                        deadline='{event.deadline}',
                                        desc='{event.desc}',
                                        org='{event.org}',
                                        signup='{event.signup}',
                                        maybe='{event.maybe}',
                                        absent='{event.absent}',
                                        categs='{event.categs}',
                                        status='{event.status}'
                                    WHERE eventid='{event.eventid}'
                                """)
    await lcon.commit()
    return None

@dbopenconnect_decor
async def db_updateCateg(lcon, cat: pclasses.Categories):
    lcurs = await lcon.execute(f"""UPDATE categories
                                    SET name='{cat.name}',
                                        elements='{cat.elements}'
                                    WHERE catid='{cat.catid}'
                                """)
    await lcon.commit()
    return None

@dbopenconnect_decor
async def db_cancelEvent(lcon, eventid: int) -> None:
    """ Updated status_id of cancelled event.
    """
    lcurs = await lcon.execute(
        f"""
        UPDATE events
        SET status={constants.EVENT_STATUS_CANCELED}
        WHERE eventid={eventid};
        """
    )
    await lcon.commit()
    return None
