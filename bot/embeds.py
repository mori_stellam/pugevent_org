import asyncio
import discord
from datetime import datetime
from zoneinfo import ZoneInfo

from discord.ext import commands

from dbmodules import classes, dbhandler
from dbmodules import config as pconfig


repoUrl = pconfig.embedUrls

emojiAcc = '✅' # b'\\u2705'
emojiMay = "❔" # b'\\u2754'
emojiRej = '✖️' # b'\\u274c'
emojiSub = ""

# ----------------------------------
# Utility functions
# ----------------------------------
def listToText(lbot: commands.Bot, thelist: str, status: int=0, compareList: str="", addstr: str=""):
    """ Returns a formatted string listing indivuals. """
    guild = lbot.get_guild(pconfig.GuildID)
    if thelist != "":
        clist = thelist.split(";")
        returnText = ""
        j = 1
        for id in clist:
            if id!="":
                laddstr = ""
                if status>=1:
                    if id in compareList:
                        laddstr = addstr
                user = lbot.get_guild(pconfig.GuildID).get_member(int(id))
                if not(user is None):
                    returnText += f"\n{j}. {user.display_name} {laddstr}"
                    j = j + 1
    else:
        returnText = "\n-----"
    return returnText[1:]

def strptimezone(text: str, format: str):
    return datetime.strptime(text, format).replace(tzinfo=ZoneInfo(pconfig.IANAZone))


# ----------------------------------
# Embeds
# ----------------------------------
def getSingleEmbed(lbot: commands.Bot, msg: str):
    theEmbed = discord.Embed(title="Feedback", url=repoUrl, description=" ")
    theEmbed.set_author(name=lbot.user.name, url=repoUrl, icon_url=lbot.user.avatar.url)
    theEmbed.add_field(name="_", inline=False, value = msg)
    return theEmbed

async def getEventEmbed(lbot: commands.Bot,  EventID: int):
    theEvent = await dbhandler.db_getEvent(EventID)
    eventCatIDList = theEvent.categs.split(";")
    readyCheckCategory = await dbhandler.db_getCat(eventCatIDList[0])
    organizer = lbot.get_guild(pconfig.GuildID).get_member(int(theEvent.org))
    eday = strptimezone(theEvent.date, pconfig.datetimeFmt)
    dday = strptimezone(theEvent.deadline, pconfig.datetimeFmt)
    sgnstring = listToText(lbot, theEvent.signup, status=theEvent.status,
                            compareList=readyCheckCategory.elements, addstr=emojiAcc)
    maybstrin = listToText(lbot, theEvent.maybe)
    absstring = listToText(lbot, theEvent.absent)

    if theEvent.status==2:
        stagedescription = '**READY CHECK PHASE**'
    else:
        stagedescription = '___________________________________'

    description = f"**Date and time** \n" + \
                  f"Event time and deadline for sign-up: {datetime.strftime(eday, pconfig.datetimeFmt)} ({eday.tzname()})\n" + \
                  f"Ready-check beginning: {datetime.strftime(dday, pconfig.datetimeFmt)} ({dday.tzname()})\n" + \
                  f"**Announcement** \n {theEvent.desc} \n " + \
                  f"**Participants** \n {stagedescription}"

    theEmbed = discord.Embed(title=f"{theEvent.name} (ID: {theEvent.eventid})", url=repoUrl, description=description)
    theEmbed.set_author(name=organizer.name, url=repoUrl, icon_url=organizer.avatar.url)
    
    if theEvent.status<2:
        theEmbed.add_field(name="Will be here", inline=True, value=sgnstring)
        theEmbed.add_field(name="Maybe", inline=True, value=maybstrin)
        theEmbed.add_field(name="Cannot make it", inline=True, value=absstring)
    else:
        theEmbed.add_field(name="Signed-up", inline=True, value=sgnstring)
        theEmbed.add_field(name="Substitute", inline=True, value=maybstrin)
        theEmbed.add_field(name="Cannot make it", inline=True, value=absstring)
        
    if theEvent.status==0:
        theEmbed.set_footer(text=f"Sign-up stage. \n{emojiAcc} - Sign-up\n{emojiMay} - Maybe\n{emojiRej} - Absent")
    elif theEvent.status==1:
        theEmbed.set_footer(text="Deadline for sign-up is over.")
    elif theEvent.status==2:
        theEmbed.set_footer(text=f"Ready-check stage. \n{emojiAcc} - Readycheck\n{emojiMay} - Substitute\n{emojiRej} - Absent")
    elif theEvent.status==3:
        theEmbed.set_footer(text=f"Event is on-going.")
    elif theEvent.status>=100:
        theEmbed.set_footer(text=f"The event is over.")
    return theEmbed

async def readycheckembed(lbot: commands.Bot,  EventID: int, eventItem: classes.EventQueueItem, sign: bool):
    theEvent = await dbhandler.db_getEvent(EventID)
    theGuild = lbot.get_guild(pconfig.GuildID)
    theChann = theGuild.get_channel(eventItem.channelid)
    organizer = theGuild.get_member(int(theEvent.org))
    if sign:
        playerstatus = "participate"
        maybetext = ""
    else:
        playerstatus = "might participate"
        maybetext = f"- Until you ready-check, the bot will indicate you as a non-participant."


    descrp = f"- You indicated that you {playerstatus} the event.\n" + \
             f"- Please check in in the `{theChann.name}` channel of the `{theGuild.name}` server." + \
             maybetext
    theEmbed = discord.Embed(title=f"Readycheck for {theEvent.name} in {theGuild.name}", url=repoUrl, description=descrp)
    theEmbed.set_author(name=organizer.name, url=repoUrl, icon_url=organizer.avatar.url)
    return theEmbed
