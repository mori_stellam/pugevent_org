import asyncio
import logging
from datetime import timedelta

import discord
from discord.commands import Option, slash_command
from discord.ext import commands

from bot import embeds as boeds
from bot import utilities as botutils
from dbmodules import config as pconfig
from dbmodules import classes as pclasses

logger = logging.getLogger('DCBOT')

timeSleep = pconfig.timeSleep

class CommandsCog(commands.Cog):
    """ Cogs for commands from discord. """

    def __init__(self, abot):
        self.bot = abot
        self.name = "bot.commands"
 
    @slash_command(guild_ids=[pconfig.GuildID])
    async def addmodrole(self, ctx: discord.ApplicationContext,
                therole: Option(discord.Role, "Select a role.")):
        """ Command to add discord roles as bot mod. """
        logger.info(">>> Request to initialize or change admin role.")

        if (pconfig.AdminRole<0 and ctx.guild.id==pconfig.GuildID):
            pconfig.AdminRole = therole.id
            pconfig.parsedParams["DCSERVER"]["AdminRole"] = str(therole.id)
            with open('config.ini', 'w') as cfgfile:
                pconfig.parsedParams.write(cfgfile)
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Admin role initialized."))

        elif pconfig.AdminRole>0 and ctx.guild.id==pconfig.GuildID:
            if pconfig.AdminRole in [role.id for role in ctx.author.roles]:
                pconfig.AdminRole = therole.id
                pconfig.parsedParams["DCSERVER"]["AdminRole"] = str(therole.id)
                with open('config.ini', 'w') as cfgfile:
                    pconfig.parsedParams.write(cfgfile)
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Admin role changed."))
            else:
                theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "AdminRole can only changed by admin."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Wrong server."))

        await asyncio.sleep(timeSleep)
        await theMsg.delete()
        return None


    @slash_command(guild_ids=[pconfig.GuildID])
    async def createevent(self, ctx: discord.ApplicationContext,
            ename: Option(str, "Name of the event"),
            edate: Option(str, "Date of the event in `dd-mm-yy HH:MM` format."),
            edeadline: Option(str, "(Optional): Deadline to sign up in `dd-mm-yy HH:MM` format.")=""):
        logger.info(" >>> Request to start an event.")
        await ctx.defer()

        if edeadline == "":
            edeadlineObj = botutils.strptimezone(edate, pconfig.datetimeFmt) - timedelta(days=1)
            edeadline = edeadlineObj.strftime(pconfig.datetimeFmt)
            del edeadlineObj
        [eventMessage, event] = await botutils.createEvent(self.bot, ctx, ename, edate, edeadline)
        feedback = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, "Event created."))
        await asyncio.sleep(pconfig.timeSleep)
        await feedback.delete()
        return None


    @slash_command(guild_ids=[pconfig.GuildID])
    async def seteventdescription(self, ctx: discord.ApplicationContext,
                                eventid: Option(int, "ID of the event."),
                                edescr: Option(str, "New description for event")):
        """ Command to change event description. """
        logger.info(">>> Request to change description of event. ")
        await ctx.defer()
        returnValue = await botutils.setEventDescription(self.bot, eventid, edescr)
        if returnValue==101:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Could not find event with ID {eventid}"))
        elif returnValue==102:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Event with ID {eventid} is over, cannot modify."))
        elif returnValue==103:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Could not find event with ID {eventid} in active event queue."))
        elif returnValue==100:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Event with ID {eventid} updated."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Unknown outcome."))
        await asyncio.sleep(pconfig.timeSleep)
        await theMsg.delete()

    @slash_command(guild_ids=[pconfig.GuildID])
    async def addeventdescription(self, ctx: discord.ApplicationContext,
                                eventid: Option(int, "ID of the event."),
                                edescr: Option(str, "New description for event")):
        """ Command to change event description. """
        logger.info(">>> Request to change description of event. ")
        await ctx.defer()
        returnValue = await botutils.addEventDescription(self.bot, eventid, edescr)
        if returnValue==101:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Could not find event with ID {eventid}"))
        elif returnValue==102:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Event with ID {eventid} is over, cannot modify."))
        elif returnValue==103:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Could not find event with ID {eventid} in active event queue."))
        elif returnValue==100:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Event with ID {eventid} updated."))
        else:
            theMsg = await ctx.respond(embed=boeds.getSingleEmbed(self.bot, f"Unknown outcome."))
        await asyncio.sleep(pconfig.timeSleep)
        await theMsg.delete()


    @slash_command(guild_ids=[pconfig.GuildID])
    async def cancelevent(
        self, ctx: discord.ApplicationContext,
        eventid: Option(int, "ID of event to be cancelled.")
    ):
        """ Command to cancel event and delete corresponding message. """
        logger.info(f">>> Request to canccel event {eventid}.")
        await ctx.defer()
        response_value = await botutils.cancel_event(self.bot, eventid)
        if response_value==101:
            theMsg = await ctx.respond(
                embed=boeds.getSingleEmbed(
                    self.bot, f"Could not find event with ID {eventid}"
                )
            )
        elif response_value==101:
            theMsg = await ctx.respond(
                embed=boeds.getSingleEmbed(
                    self.bot, f"Event with ID {eventid} is not active."
                )
            )
        elif response_value==100:
            theMsg = await ctx.respond(
                embed=boeds.getSingleEmbed(
                    self.bot, f"Event with ID {eventid} is cancelled."
                )
            )
        else:
            theMsg = await ctx.respond(
                embed=boeds.getSingleEmbed(self.bot, f"Unknown outcome.")
            )
        await asyncio.sleep(pconfig.timeSleep)
        await theMsg.delete()
        

    # -------------------
    # Test commands
    # -------------------
    @slash_command(guild_ids=[pconfig.GuildID])
    async def testbutton(self, ctx: discord.ApplicationContext):
        logger.info(">>> Test button command issued.")
        embed=discord.Embed(title="Test embed", url="https://numeraire92.ddns.net", description="Example description", color=0xff0000)
        embed.set_author(name="Test author")
        embed.add_field(name="Example field", value="Example value", inline=False)
        embed.set_footer(text="Example footer")
        await ctx.respond(content="TestMsg", embed=embed, view=botutils.SignupView())
        return None

    @slash_command(guild_ids=[pconfig.GuildID])
    async def listmessages(self, ctx: discord.ApplicationContext):
        await ctx.defer()
        await botutils.listMessages(self.bot, ctx)
        await ctx.respond("Thank you.")
        return None


def setup(bot):
    bot.add_cog(CommandsCog(bot))