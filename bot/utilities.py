import asyncio
import os
import random
from datetime import datetime, timedelta
from zoneinfo import ZoneInfo

import discord
from discord.ext import commands

from bot import embeds as boeds
from bot import constants
from dbmodules import config as pconfig
from dbmodules import classes, dbhandler

emojiAcc = '✅' # b'\\u2705'
emojiMay = "❔" # b'\\u2754'
emojiRej = '✖️' # b'\\u274c'


# ----------------------------------------
# Supporting functions
# ----------------------------------------
def getEventQueue():
    """ Returns the list of active events """

    theList = []
    if os.path.exists("EventQueue.txt"):
        with open("EventQueue.txt") as file:
            Lines = file.readlines()
        for line in Lines:
            theList.append(classes.EventQueueItem(line))
    return theList


def getEventItemFromE(theEvent: classes.Event):
    """ Returns `EventItem' corresponding to event."""
    EVENTSQUEUE = getEventQueue()
    eventItem = None
    i = 0
    while (i+1<=len(EVENTSQUEUE)) and (eventItem is None):
        if EVENTSQUEUE[i].eventid==theEvent.eventid:
            eventItem = EVENTSQUEUE[i]
        i+=1
    return eventItem


def dumpEventQueue(eventQueue: list):
    """ Write eventQueue to textfile. """
    with open("EventQueue.txt", 'w') as file:
        for EventItem in eventQueue:
            file.write(EventItem.getStoreString() + "\n")
    return None


def removeEventItem(theEvent: classes.Event):
    """ Remove event from event queue """
    eventItemQueue = getEventQueue()
    eventItem = getEventItemFromE(theEvent)
    newlist = []
    for nextItem in eventItemQueue:
        if not(nextItem==eventItem):
            newlist.append(nextItem)
    if len(newlist)>0:
        dumpEventQueue(eventItemQueue)
    else:
        with open("EventQueue.txt", 'w') as file:
            pass
    return None


def addToList(thelist: str, element: int):
    """ Append item to list stored in string. """
    if thelist != "":
        clist = thelist.split(";")
        clist.append(str(element))
        retList = ";".join(clist)
    else:
        retList = f"{str(element)}"
    return retList


def removeFromList(thelist: str, element: int):
    """ Remove item from list stored in string. """
    clist = thelist.split(";")
    clist.remove(str(element))
    return ";".join(clist)


async def getNextFreeCatID():
    """ Returns next free ID for categories."""
    CatList = await dbhandler.db_getAllCats()
    CatIDList = [cat.catid for cat in CatList]
    retid = 1
    if len(CatIDList)>0:
        while retid in CatIDList:
            retid += 1
    return retid


async def getFreeEventID():
    """ Returns a new unused ID for an event """
    newEventID = random.randrange(10000,1000000)
    theEvent = await dbhandler.db_getEvent(newEventID, False)
    while theEvent is not None:
        newEventID = random.randrange(10000, 99999)
        theEvent = await dbhandler.db_getEvent(newEventID, False)
    return newEventID

def strptimezone(text: str, format: str):
    return datetime.strptime(text, format).replace(tzinfo=ZoneInfo(pconfig.IANAZone))


# ----------------------------------------
# Task / Event functions
# ----------------------------------------
async def checkEventStatus(lbot: commands.Bot):
    """ 
    Loops over the EventQueue and updates the status of
    events if necessary.
    """
    currEventQueue = getEventQueue()
    for eventItem in currEventQueue:
        theEvent = await dbhandler.db_getEvent(eventItem.eventid)
        theChan = lbot.get_guild(pconfig.GuildID).get_channel(eventItem.channelid)
        theMsg = await theChan.fetch_message(eventItem.messageid)
        #print(f"Found event {theEvent.name} with status {theEvent.status}.")
        now = datetime.now(ZoneInfo(pconfig.IANAZone))
        deadline = strptimezone(theEvent.deadline, pconfig.datetimeFmt)
        evedate = strptimezone(theEvent.date, pconfig.datetimeFmt)
        #print(f"{datetime.strftime(now, pconfig.datetimeFmt)} - {datetime.strftime(deadline, pconfig.datetimeFmt)}")
        if theEvent.status==0 and deadline<now:
            print(f"Increase status of event {theEvent.name} with status {theEvent.status}.")
            theEvent.status = 1
            await dbhandler.db_updateEvent(theEvent)
            await repostActiveEvent(lbot, theEvent.eventid)
        elif theEvent.status<=1 and timedelta(days=pconfig.readyCheckDays)>(evedate-now):
            theEvent.status=2
            await DMForReadyCheck(lbot, theEvent.eventid)
            await dbhandler.db_updateEvent(theEvent)
            await repostActiveEvent(lbot, theEvent.eventid)
        elif theEvent.status<100 and now>evedate:
            theEvent.status=100
            await dbhandler.db_updateEvent(theEvent)
            await repostActiveEvent(lbot, theEvent.eventid)
            await removeEventItemFromQueue(lbot, theEvent.eventid)
    return None


async def repostActiveEvents(lbot: commands.Bot):
    """ Identifies active events and reposts them when bot is ready.
    """
    guild = lbot.get_guild(pconfig.GuildID)
    newQueue = []
    for eventItem in getEventQueue():
        channel = guild.get_channel(eventItem.channelid)
        event = await dbhandler.db_getEvent(eventItem.eventid, False)
        try:
            message = await channel.fetch_message(eventItem.messageid)
            await message.delete()
        except:
            pass
        
        theEmbed = await boeds.getEventEmbed(lbot, event.eventid)
        if event.status==0:
            theView = SignupView(bot=lbot, event=event)
        elif event.status==1 or event.status>2:
            theView = None
        elif event.status==2:
            theView = ReadyCheckView(bot=lbot, event=event)
        else:
            theView = None
        theMsg = await channel.send(embed=theEmbed, view=theView)
        eventItem.messageid = theMsg.id
        newQueue.append(eventItem)
    dumpEventQueue(newQueue)
    return None

async def repostActiveEvent(lbot: commands.Bot, eventid: int):
    """ Reposts active event to its channel. """
    EVENTQUEUE = getEventQueue()
    eventItem = None
    i = 0
    while i+1<=len(EVENTQUEUE) and eventItem is None:
        if eventid == EVENTQUEUE[i].eventid:
            eventItem = EVENTQUEUE[i]
        i += 1
    if not(eventItem is None):
        event = await dbhandler.db_getEvent(eventItem.eventid, False)
        await delete_message_by_event_item(lbot, eventItem)

        theEmbed = await boeds.getEventEmbed(lbot, event.eventid)
        if event.status==0:
            theView = SignupView(bot=lbot, event=event)
        elif event.status==1 or event.status>2:
            theView = None
        elif event.status==2:
            theView = ReadyCheckView(bot=lbot, event=event)
        else:
            theView = None
        
        channel = lbot.get_guild(pconfig.GuildID).get_channel(eventItem.channelid)
        theMsg = await channel.send(embed=theEmbed, view=theView)
        EVENTQUEUE[i-1].messageid = theMsg.id
        dumpEventQueue(EVENTQUEUE)
    return None

async def delete_message_by_event_item(
    lbot: commands.Bot, event_item: classes.EventQueueItem
) -> bool:
    """ Deletes the message corresponding to event_item from channel. """
    channel = (
        lbot
        .get_guild(pconfig.GuildID)
        .get_channel(event_item.channelid)
    )
    try:
        message = await channel.fetch_message(event_item.messageid)
        await message.delete()
        return True
    except:
        return False

async def DMForReadyCheck(lbot: commands.Bot, eventid: int):
    """ Loops over the list of signed-up or maybe participants and DM-s them. """
    event = await dbhandler.db_getEvent(eventid)
    eventItem = getEventItemFromE(event)
    flag = False
    for thelist in [event.signup, event.maybe]:
        flag = not flag
        if len(thelist)>0:
            convList = [int(i) for i in thelist.split(";")]
            theEmbed = await boeds.readycheckembed(lbot, eventid, eventItem, flag)
            for id in convList:
                participant = lbot.get_guild(pconfig.GuildID).get_member(id)
                if not(participant is None):
                    await participant.send(embed=theEmbed)
    return None


async def removeEventItemFromQueue(lbot: commands.Bot, eventid: int):
    """ Removes finished events from the queue."""
    event = await dbhandler.db_getEvent(eventid)
    eventItem = getEventItemFromE(event)
    returnList = getEventQueue()
    if not(eventItem is None):
        if event.status>=constants.EVENT_STATUS_CANCELED:
            returnList.remove(eventItem)
    dumpEventQueue(returnList)
    return None


# ----------------------------------------
# Functions to resolve commands
# ----------------------------------------
async def createEvent(lbot: commands.Bot,
                    ctx: discord.ApplicationContext,
                    ename: str, edate: str, edeadline: str):
    """
    Creates a new event and a corresponding reacy check category.
    """
    newEventID = await getFreeEventID()
    theEvent = classes.Event(newEventID, ename, edate, edeadline,
                            "Placeholder description", ctx.user.id,
                            "", "", "", "", 0)
    readyCheckID = await getNextFreeCatID()
    theCategory = classes.Categories(readyCheckID, newEventID, "ReadyCheck", "")
    theEvent.categs = f"{readyCheckID}"
    await dbhandler.db_addEvent(theEvent)
    await dbhandler.db_addCat(theCategory)

    theEmbed = await boeds.getEventEmbed(lbot, theEvent.eventid)
    theView = SignupView(event=theEvent, bot=lbot)
    theMsg = await ctx.send(embed=theEmbed, view=theView)

    EVENTSQUEUE = getEventQueue()
    EVENTSQUEUE.append(classes.EventQueueItem(theMsg.channel.id,
                                                theMsg.id,
                                                theEvent.eventid))
    dumpEventQueue(EVENTSQUEUE)
    return [theMsg, theEvent]


async def setEventDescription(lbot: commands.Bot, eventid: int, descr: str):
    """ Changes the description of a specified event. """
    theEvent = await dbhandler.db_getEvent(eventid)
    if not(theEvent is None):
        if theEvent.status<100:
            theEvent.desc = descr
            await dbhandler.db_updateEvent(theEvent)
            EventItem = getEventItemFromE(theEvent)

            if EventItem is None:
                returnValue = 103
            else:
                theChannel = lbot.get_guild(pconfig.GuildID).get_channel(EventItem.channelid)
                theMsg = await theChannel.fetch_message(EventItem.messageid)
                theEmbed = await boeds.getEventEmbed(lbot, theEvent.eventid)
                await theMsg.edit(embed=theEmbed)
                return 100
        else:
            returnValue = 102
    else:
        returnValue = 101
    return returnValue


async def addEventDescription(lbot: commands.Bot, eventid: int, descr: str):
    """ Add to the description of a specified event. """
    theEvent = await dbhandler.db_getEvent(eventid)
    if not(theEvent is None):
        if theEvent.status<100:
            theEvent.desc = theEvent.desc + "\n" + descr
            await dbhandler.db_updateEvent(theEvent)
            EventItem = getEventItemFromE(theEvent)

            if EventItem is None:
                returnValue = 103
            else:
                theChannel = lbot.get_guild(pconfig.GuildID).get_channel(EventItem.channelid)
                theMsg = await theChannel.fetch_message(EventItem.messageid)
                theEmbed = await boeds.getEventEmbed(lbot, theEvent.eventid)
                await theMsg.edit(embed=theEmbed)
                return 100
        else:
            returnValue = 102
    else:
        returnValue = 101
    return returnValue


async def cancel_event(lbot: commands.Bot, eventid: int) -> int:
    """
    Cancels the specified event if it was active.
    Args:
    - lbot: Bot.
    - eventid: ID of the event to be canceled
    Returns:
    - ID of outcome.
    """

    event = await dbhandler.db_getEvent(eventid)
    if event:
        if event.status < constants.EVENT_STATUS_CANCELED:
            # Check and update event status
            await dbhandler.db_cancelEvent(event.eventid)

            # Check and remove event from Queue
            event_queue_item = getEventItemFromE(event)
            if event_queue_item:
                await delete_message_by_event_item(lbot, event_queue_item)
                await removeEventItemFromQueue(lbot, event.eventid)

            return 100 # Success
        else:
            return 102 # Non-active event
    else:
        return 101 # Cannot find event by id


# ----------------------------------------
# VIEWES to resolve interactions
# ----------------------------------------
class SignupView(discord.ui.View):
    def __init__(self, event: classes.Event=None, bot: commands.Bot=None):
        super().__init__(timeout=None)
        self.event = event
        self.bot = bot

    @discord.ui.button(style=discord.ButtonStyle.green, custom_id=f"sign_1", emoji=emojiAcc)
    async def greenButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)
        if not(str(author.id) in self.event.signup):
            self.event.signup = addToList(self.event.signup, author.id)
        if str(author.id) in self.event.maybe:
            self.event.maybe = removeFromList(self.event.maybe, author.id)
        if str(author.id) in self.event.absent:
            self.event.absent = removeFromList(self.event.absent, author.id)
        await dbhandler.db_updateEvent(self.event)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None
    
    @discord.ui.button(style=discord.ButtonStyle.grey, custom_id=f"sign_2", emoji=emojiMay)
    async def greyButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)
        if not(str(author.id) in self.event.maybe):
                self.event.maybe = addToList(self.event.maybe, author.id)
        if str(author.id) in self.event.signup:
            self.event.signup = removeFromList(self.event.signup, author.id)
        if str(author.id) in self.event.absent:
            self.event.absent = removeFromList(self.event.absent, author.id)
        await dbhandler.db_updateEvent(self.event)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None

    @discord.ui.button(style=discord.ButtonStyle.red, custom_id=f"sign_3", emoji=emojiRej)
    async def redButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)
        if not(str(author.id) in self.event.absent):
            self.event.absent = addToList(self.event.absent, author.id)
        if str(author.id) in self.event.maybe:
            self.event.maybe = removeFromList(self.event.maybe, author.id)
        if str(author.id) in self.event.signup:
            self.event.signup = removeFromList(self.event.signup, author.id)
        await dbhandler.db_updateEvent(self.event)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None


class ReadyCheckView(discord.ui.View):
    def __init__(self, event: classes.Event=None, bot: commands.Bot=None):
        super().__init__(timeout=None)
        self.event = event
        self.bot = bot
        self.readycheck = None

    @discord.ui.button(style=discord.ButtonStyle.green, custom_id=f"rcheck_1", emoji=emojiAcc)
    async def greenButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        eventCatIDList = self.event.categs.split(";")
        self.readycheck = await dbhandler.db_getCat(eventCatIDList[0])
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)

        if not(str(author.id) in self.readycheck.elements):
            if not(str(author.id) in self.event.signup):
                self.event.signup = addToList(self.event.signup, author.id)
            if str(author.id) in self.event.maybe:
                self.event.maybe = removeFromList(self.event.maybe, author.id)
            if str(author.id) in self.event.absent:
                self.event.absent = removeFromList(self.event.absent, author.id)
            self.readycheck.elements = addToList(self.readycheck.elements, author.id)
            await dbhandler.db_updateEvent(self.event)
            await dbhandler.db_updateCateg(self.readycheck)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None

    @discord.ui.button(style=discord.ButtonStyle.grey, custom_id=f"rcheck_2", emoji=emojiMay)
    async def greyButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        eventCatIDList = self.event.categs.split(";")
        self.readycheck = await dbhandler.db_getCat(eventCatIDList[0])
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)

        if not(str(author.id) in self.readycheck.elements):
            if not(str(author.id) in self.event.maybe):
                    self.event.maybe = addToList(self.event.maybe, author.id)
            if str(author.id) in self.event.signup:
                self.event.signup = removeFromList(self.event.signup, author.id)
            if str(author.id) in self.event.absent:
                self.event.absent = removeFromList(self.event.absent, author.id)
            await dbhandler.db_updateEvent(self.event)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None

    @discord.ui.button(style=discord.ButtonStyle.red, custom_id=f"rcheck_3", emoji=emojiRej)
    async def redButton(self, button: discord.ui.Button, interact: discord.Interaction):
        self.event = await dbhandler.db_getEvent(self.event.eventid)
        eventCatIDList = self.event.categs.split(";")
        self.readycheck = await dbhandler.db_getCat(eventCatIDList[0])
        author = self.bot.get_guild(pconfig.GuildID).get_member(interact.user.id)

        if not(str(author.id) in self.readycheck.elements):
            if not(str(author.id) in self.event.absent):
                self.event.absent = addToList(self.event.absent, author.id)
            if str(author.id) in self.event.maybe:
                self.event.maybe = removeFromList(self.event.maybe, author.id)
            if str(author.id) in self.event.signup:
                self.event.signup = removeFromList(self.event.signup, author.id)
            self.readycheck.elements = addToList(self.readycheck.elements, author.id)
            await dbhandler.db_updateEvent(self.event)
            await dbhandler.db_updateCateg(self.readycheck)
        theEmber = await boeds.getEventEmbed(self.bot, self.event.eventid)
        #await interact.message.edit(embed=theEmber)
        #await interact.response.send_message("Interaction registered.", ephemeral=True)
        await interact.response.edit_message(embed=theEmber)
        return None


# ----------------------------------------
# TEST UTILITY FUNCTIONS / VIEWS / EMBEDS
# ----------------------------------------
# class SignupView(discord.ui.View):
#     @discord.ui.button(label="Green", style=discord.ButtonStyle.green)
#     async def greenButton(self, button: discord.ui.Button, interact: discord.Interaction):
#         print("Green button pressed.")
#         theMsg = await interact.original_message()
#         print(theMsg)

#     @discord.ui.button(label="Red", style=discord.ButtonStyle.red)
#     async def redButton(self, button: discord.ui.Button, interact: discord.Interaction):
#         print("Redd button pressed.")
#         await interact.response.edit_message(view=None, delete_after=1)


async def listMessages(lbot: commands.Bot, ctx: discord.ApplicationContext):
    counter = 1
    async for message in ctx.channel.history(limit=2000):
        print(f"=============================\nMessage No. {counter}.")
        print(f">> Type: {type(message)}")
        print(f">> Message dict: ")
        print(message)
        print(f">> Components: {message.components}")
        print(f">> Content: {message.content}")
        print(f">> Embeds: ")
        if len(message.embeds)>0:
            for embed in message.embeds:
                print(embed)
                for field in embed.fields:
                    print(field)
        else:
            print(">>>> No embed")
        counter += 1
